Pod::Spec.new do |s|
	s.ios.deployment_target = '9.3'
	s.osx.deployment_target = '10.10'
	s.name             = 'benzocom'
	s.version          = '0.8.0'
	s.summary          = 'common interface parts for benzo project'
	s.requires_arc 	   = true

	s.description      = <<-DESC
Common headers used by all denti libraries
	DESC

	s.homepage         = 'https://github.com/micartu/benzocom'
	s.license          = { :type => 'MIT', :file => 'LICENSE' }
	s.author           = { 'micartu' => 'michael.artuerhof@gmail.com' }
	s.source           = { :git => 'https://github.com/micartu/benzocom.git', :tag => s.version.to_s }

	s.source_files = 'benzocom/**/*.{swift}'

	s.frameworks = 'Foundation'
	#s.dependency 'RxSwift', '~> 4.3.1'
	s.swift_version = "4.2"
end
