//
//  AppDelegateProtocol.swift
//  benzocom
//
//  Created by Michael Artuerhof on 24.05.19.
//  Copyright © 2019 Michael Artuerhof. All rights reserved.
//

import Foundation

public protocol AppDelegateProtocol {
    func description() -> String
}
