//
//  user.swift
//  benzocom
//
//  Created by Michael Artuerhof on 24.05.19.
//  Copyright © 2019 Michael Artuerhof. All rights reserved.
//

import Foundation

public struct User {
    /// user's login
    public var login: String
    /// user's phone
    public var phone: String
    /// user's name
    public var name: String
    /// user's surname
    public var surname: String
    /// user's middlename
    public var midname: String
    /// user's avatar
    public var avatar: String
    /// user's favorite fuel
    public var fuel: String
    /// user's codeword
    public var codeword: String
    /// user's card
    public var card: String
    /// user card's PIN
    public var pin: String
    /// user's email
    public var email: String
    /// user's balance
    public var balance: Double
    /// user's loyality
    public var loyality: UserLoyality
    /// status of the card (blocked or not)
    public var cardBlocked: Bool
    /// push notification token
    public var apn: String
}

public struct UserLoyality {
    // additional stuff
    public var cpa_crd_code: String
    public var cpa_crd_rub: Int
    public var cpa_prt_code: String
    public var cpa_prt_name: String
    public var cpa_prt_ref1: String
    public var cpa_prt_ref2: String
    public var cpa_prt_ref3: String
    public var cpa_prt_subid: String

    public init(cpa_crd_code: String,
         cpa_crd_rub: Int,
         cpa_prt_code: String,
         cpa_prt_name: String,
         cpa_prt_ref1: String,
         cpa_prt_ref2: String,
         cpa_prt_ref3: String,
         cpa_prt_subid: String) {
        self.cpa_crd_code = cpa_crd_code
        self.cpa_crd_rub = cpa_crd_rub
        self.cpa_prt_code = cpa_prt_code
        self.cpa_prt_name = cpa_prt_name
        self.cpa_prt_ref1 = cpa_prt_ref1
        self.cpa_prt_ref2 = cpa_prt_ref2
        self.cpa_prt_ref3 = cpa_prt_ref3
        self.cpa_prt_subid = cpa_prt_subid
    }

    public init() {
        self.init(cpa_crd_code: "",
                  cpa_crd_rub: 0,
                  cpa_prt_code: "",
                  cpa_prt_name: "",
                  cpa_prt_ref1: "",
                  cpa_prt_ref2: "",
                  cpa_prt_ref3: "",
                  cpa_prt_subid: "")
    }
}
