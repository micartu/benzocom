//
//  CacheService.swift
//  benzocom
//
//  Created by Michael Artuerhof on 24.05.19.
//  Copyright © 2019 Michael Artuerhof. All rights reserved.
//

import Foundation

public protocol CacheService {
    // common methods
    func exists(user: String, completion: @escaping ((Bool) -> Void))
    func exists(patch id: Int, completion: @escaping ((Bool) -> Void))
    func exists(transaction: String, completion: @escaping ((Bool) -> Void))
    
    // get methods
    func get(login: String, completion: @escaping ((User?) -> Void))
    
    // delete methods
    func delete(login: String, completion: @escaping (() -> Void))
}
